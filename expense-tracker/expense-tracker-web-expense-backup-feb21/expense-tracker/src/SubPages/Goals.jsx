import axios from "axios";
import React, { createContext, useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import CreateNewGoal from "../Components/CreateNewGoal";
import { valueContext } from "../Components/MainLayout";

const url = "http://127.0.0.1:5000/goals";

export const goalContext = createContext(null);

const Goals = () => {
  const [data, setData] = useState([]);
  const [open, setOpen] = useState(false);
  const [value, setValue] = useContext(valueContext);
  const navigate = useNavigate();
  const getData = async () => {
    try {
      const resp = await axios.get(url);
      const main = resp.data;
      setData(main);
    } catch (error) {
      console.log(error.response);
    }
    // setData(resp.data);
  };
  const getSpecData = async (data) => {
    try {
      const resp = await axios.get(url + "/" + data);
      const main = resp.data;
      setValue(main);
      navigate("/goalstatus");
    } catch (error) {
      console.log(error.response);
    }
  };

  console.log(value);
  useEffect(() => {
    getData();
  }, []);
  return (
    <goalContext.Provider value={[open, setOpen]}>
      <div className="w-full ">
        {open ? (
          <div>
            <CreateNewGoal />
            {/* <div>
              <button
                className="px-4 py-2 m-2 text-white transition-all rounded-md bg-slate-800 hover:scale-110"
                onClick={() => setOpen(false)}
              >
                Submit
              </button>
            </div> */}
          </div>
        ) : (
          <div className="w-full ">
            <button
              className="px-4 py-2 m-2 text-white transition-all rounded-md bg-slate-800 hover:scale-110"
              onClick={() => setOpen(true)}
            >
              Create New
            </button>
            <table className="w-full border-2">
              <tr>
                <th className="border-2 ">Category</th>
                <th className="border-2 ">Date</th>
                <th className="border-2 ">Amount</th>
                <th className="border-2 ">Time</th>
                <th className="border-2 ">Months to Achieve</th>
                <th className="border-2 ">Actions</th>
              </tr>
              {data.map((data) => (
                <tr>
                  <td className="p-3 text-center">{data.category}</td>
                  <td className="p-3 text-center">{data.date}</td>
                  <td className="p-3 text-center">{data.Amount}</td>
                  <td className="p-3 text-center">{data.time}</td>
                  <td className="p-3 text-center">
                    {data.MonthsRemaining} Months
                  </td>
                  <td className="p-3 space-x-3 text-center ">
                    <button
                      className="w-[5vw] px-4 py-2  text-white transition-all rounded-md bg-slate-800 hover:scale-110"
                      onClick={() => getSpecData(data._id)}
                    >
                      Select
                    </button>
                    <button className="w-[5vw] px-4 py-2  text-white transition-all rounded-md bg-slate-800 hover:scale-110">
                      Delete
                    </button>
                  </td>
                </tr>
              ))}
            </table>
          </div>
        )}
      </div>
    </goalContext.Provider>
  );
};

export default Goals;

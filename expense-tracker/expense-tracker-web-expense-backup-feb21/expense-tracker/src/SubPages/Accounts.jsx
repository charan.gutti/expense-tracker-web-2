import axios from "axios";
import React, { createContext, useEffect, useState } from "react";
import CreateNewRecord from "../Components/CreateNewRecord";

const url = "http://127.0.0.1:5000/expenses";
export const buttonContext = createContext(null);

const Accounts = () => {
  const [data, setData] = useState([]);
  const [open, setOpen] = useState(false);
  const getData = async () => {
    try {
      const resp = await axios.get(url);
      const main = resp.data;
      setData(main);
    } catch (error) {
      console.log(error.response);
    }
    // setData(resp.data);
  };

  useEffect(() => {
    getData();
  }, []);
  return (
    <buttonContext.Provider value={[open, setOpen]}>
      <div className="w-full ">
        {open ? (
          <div>
            <CreateNewRecord />
            <div>
              {/* <button
                className="px-4 py-2 m-2 text-white transition-all rounded-md bg-slate-800 hover:scale-110"
                onClick={() => setOpen(false)}
              >
                Submit
              </button> */}
            </div>
          </div>
        ) : (
          <div className="w-full ">
            <button
              className="px-4 py-2 m-2 text-white transition-all rounded-md bg-slate-800 hover:scale-110"
              onClick={() => setOpen(true)}
            >
              Create New
            </button>
            <table className="w-full border-2">
              <tr>
                <th className="border-2 ">Category</th>
                <th className="border-2 ">Date</th>
                <th className="border-2 ">Expense</th>
                <th className="border-2 ">Time</th>
                <th className="border-2 ">Actions</th>
              </tr>
              {data.map((data) => (
                <tr>
                  <td className="p-3 text-center">{data.category}</td>
                  <td className="p-3 text-center">{data.date}</td>
                  <td className="p-3 text-center">{data.expense}</td>
                  <td className="p-3 text-center">{data.time}</td>
                  <td className="p-3 space-x-3 text-center ">
                    <button className="w-[5vw] px-4 py-2  text-white transition-all rounded-md bg-slate-800 hover:scale-110">
                      Select
                    </button>
                    <button className="w-[5vw] px-4 py-2  text-white transition-all rounded-md bg-slate-800 hover:scale-110">
                      Delete
                    </button>
                  </td>
                </tr>
              ))}
            </table>
          </div>
        )}
      </div>
    </buttonContext.Provider>
  );
};

export default Accounts;

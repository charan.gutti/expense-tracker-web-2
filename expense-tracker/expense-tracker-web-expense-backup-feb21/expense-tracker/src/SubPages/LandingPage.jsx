import { BarChart } from "@mui/x-charts/BarChart";
import { LineChart } from "@mui/x-charts/LineChart";
import { PieChart } from "@mui/x-charts/PieChart";
import axios from "axios";

import React, { useEffect, useState } from "react";

const url1 = "http://127.0.0.1:5000/dailyexpenses";

const url2 = "http://127.0.0.1:5000/totalexpenses";

const url3 = "http://127.0.0.1:5000/monthlyexpenses";

const url = "http://127.0.0.1:5000/expenses";

const Home = () => {
  const [daily, setDaily] = useState();
  const [total, setTotal] = useState();
  const [monthly, setMonthly] = useState([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);
  const [expenses, setExpenses] = useState([]);
  const getData = async () => {
    try {
      const resp = await axios.get(url);
      const main = resp.data;
      setExpenses(main);
    } catch (error) {
      console.log(error.response);
    }
    // setData(resp.data);
  };
  const getDailyExp = async () => {
    try {
      const resp = await axios.get(url1);
      const data = resp.data;
      setDaily(data);
    } catch (error) {
      console.log(error.resp);
    }
  };

  const getTotalExp = async () => {
    try {
      const resp = await axios.get(url2);
      const data = resp.data;
      setTotal(resp.data);
    } catch (error) {
      console.log(error.resp);
    }
  };

  const MonthlyExp = async () => {
    try {
      const resp = await axios.get(url3);
      const data = resp.data;
      const monthly = data;
      setMonthly(monthly);
    } catch (error) {
      console.log(error.resp);
    }
  };

  useEffect(() => {
    getDailyExp();
    getTotalExp();
    MonthlyExp();
    getData();
  }, []);

  const data = [
    { id: 0, value: 10, label: "series A" },
    { id: 1, value: 15, label: "series B" },
    { id: 0, value: 10, label: "series A" },
    { id: 1, value: 15, label: "series B" },
    { id: 2, value: 20, label: "series C" },
  ];

  const pieData = [];

  expenses.map((data) => {
    pieData.push({
      id: data.category,
      value: data.expense,
      label: data.category,
    });
  });
  const uData = [4000, 3000, 2000, 2780, 1890, 2390, 3490];
  const pData = [2400, 1398, 9800, 3908, 4800, 3800, 4300];
  const xLabels = [
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
    "Sunday",
  ];
  return (
    <div className="w-full">
      <div className="flex flex-row w-full justify-evenly ">
        <div className="flex justify-center w-1/5 p-10 space-x-2 border-2 rounded-md">
          <p className="font-bold">Daily Expenses:</p>
          <p>{daily}</p>
        </div>
        <div className="flex justify-center w-1/5 p-10 space-x-2 border-2 rounded-md">
          <p className="font-bold ">Total Expenses:</p>
          <p>{total}</p>
        </div>
        <div className="flex justify-center w-1/5 p-10 space-x-2 border-2 rounded-md">
          <p className="font-bold ">Goals Avg Spend</p>
        </div>
        <div className="flex justify-center w-1/5 p-10 space-x-2 border-2 rounded-md">
          <p className="font-bold ">Expense Prediction</p>
        </div>
      </div>
      <div className="flex flex-wrap justify-center w-full my-4">
        <div className="w-1/2 p-4 border-2 rounded-lg ">
          <BarChart
            series={[{ data: monthly }]}
            height={290}
            xAxis={[
              {
                data: [
                  "Jan",
                  "Feb",
                  "March",
                  "Apr",
                  "May",
                  "Jun",
                  "Jul",
                  "Aug",
                  "Sept",
                  "Oct",
                  "Nov",
                  "Dec",
                ],
                scaleType: "band",
              },
            ]}
            margin={{ top: 10, bottom: 30, left: 40, right: 10 }}
          />
        </div>
        <div className="w-1/2 p-4 border-2 rounded-lg ">
          <LineChart
            xAxis={[{ data: [1, 2, 3, 5, 8, 10] }]}
            series={[
              {
                data: [2, 5.5, 2, 8.5, 1.5, 5],
              },
            ]}
            height={300}
          />
        </div>
        <div className="flex justify-center w-1/2 h-full p-4 align-middle border-2 rounded-lg">
          <PieChart
            series={[
              {
                data: pieData,
                highlightScope: { faded: "global", highlighted: "item" },
                faded: {
                  innerRadius: 30,
                  additionalRadius: -30,
                  color: "gray",
                },
              },
            ]}
            height={300}
          />
        </div>
        <div className="w-1/2 p-4 border-2 rounded-lg ">
          <LineChart
            height={300}
            series={[
              { data: pData, label: "Ideal" },
              { data: uData, label: "Expense" },
            ]}
            xAxis={[{ scaleType: "point", data: xLabels }]}
          />
          {console.log(pieData)}
        </div>
      </div>
    </div>
  );
};

export default Home;

import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { valueContext } from "../Components/MainLayout";

const url = "http://127.0.0.1:5000/goalstatus";
const GoalsStatus = () => {
  const [value, setValue] = useContext(valueContext);
  const [idealSpend, setIdeal] = useState(0);
  console.log(value);
  const postData = async () => {
    try {
      const resp = await axios.post(url, {
        Amount: value.Amount,
        MonthsRemaining: value.MonthsRemaining,
      });
      const main = resp.data;
      setIdeal(main);
    } catch (error) {
      console.log(error.response);
    }
  };

  useEffect(() => {
    postData();
  }, []);
  return (
    <div>
      GoalsStatus
      <p>Possibility %:</p> <p>Avg Spend per Month:{idealSpend}</p>
      <p> completes in how many month:</p>
      <p>graph</p>
      <p> start the goal</p>
      <button
        className="w-[5vw] px-4 py-2 m-2 text-white transition-all rounded-md bg-slate-800 hover:scale-110"
        onClick={postData}
      >
        Check
      </button>
    </div>
  );
};

export default GoalsStatus;

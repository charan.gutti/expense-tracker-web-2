import React from "react";

const Home = React.lazy(() => import("../SubPages/LandingPage"));
const Accounts = React.lazy(() => import("../SubPages/Accounts"));
const Goals = React.lazy(() => import("../SubPages/Goals"));
const Goalstatus = React.lazy(() => import("../SubPages/GoalsStatus"));

const routes = [
  { name: "Home", path: "/home", element: Home },
  { name: "Accounts", path: "/accounts", element: Accounts },
  { name: "Goals", path: "/goals", element: Goals },
  { name: "GoalsStatus", path: "/goalstatus", element: Goalstatus },
];

export default routes;

import React from "react";
import MainLayout from "../Components/MainLayout";
import SideBar from "../Components/SideBar";

const Home = () => {
  return (
    <div className="flex flex-row">
      <div>
        <SideBar />
      </div>
      <div className="flex w-full p-4 border-2 border-black">
        <MainLayout />
      </div>
    </div>
  );
};

export default Home;

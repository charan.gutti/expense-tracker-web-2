import React, { Suspense, createContext, useState } from "react";
import { Navigate, Route, Routes } from "react-router-dom";
import routes from "../Datasets/routes";

export const valueContext = createContext(null);
const MainLayout = () => {
  const [value, setValue] = useState();
  return (
    <Suspense>
      <valueContext.Provider value={[value, setValue]}>
        <Routes>
          {routes.map((route, idx) => {
            return (
              route.element && (
                <Route
                  key={idx}
                  name={route.name}
                  path={route.path}
                  element={<route.element />}
                />
              )
            );
          })}
          <Route path="/" element={<Navigate to="home" replace />} />
        </Routes>
      </valueContext.Provider>
    </Suspense>
  );
};

export default MainLayout;

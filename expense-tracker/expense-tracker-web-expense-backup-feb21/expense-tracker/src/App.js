import { Component, Suspense } from "react";
import { HashRouter, Route, Routes } from "react-router-dom";
import Home from "./Pages/Home";
import Login from "./Pages/Login";

class App extends Component {
  render() {
    return (
      <HashRouter>
        <Suspense>
          <Routes>
            <Route path="/login" name="Login" element={<Login />} />
            <Route path="*" name="Home" element={<Home />} />
          </Routes>
        </Suspense>
      </HashRouter>
    );
  }
}

export default App;
